package edu.westga.cs.schoolgrades.controllers;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import edu.westga.cs.schoolgrades.model.AverageOfGradesStrategy;
import edu.westga.cs.schoolgrades.model.CompositeGrade;
import edu.westga.cs.schoolgrades.model.DropLowestStrategy;
import edu.westga.cs.schoolgrades.model.Grade;
import edu.westga.cs.schoolgrades.model.SimpleGrade;
import edu.westga.cs.schoolgrades.model.SumOfGradesStrategy;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.ListView;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.TextFieldListCell;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;

/**
 * This class will define the GUI
 * 
 * @author Hayden Smith
 * 
 * @version 11/1/17
 *
 */
public class SchoolGradesController implements Initializable {
	@FXML
	private ListView<String> quizGrade;
	@FXML
	private ListView<String> hwGrade;
	@FXML
	private ListView<String> examGrade;
	@FXML
	private TextField quizTotal;
	@FXML
	private TextField hwTotal;
	@FXML
	private TextField examTotal;
	@FXML
	private TextField finalGrade;
	@FXML
	private Button recalculate;
	@FXML
	private MenuItem addQuiz;
	@FXML
	private MenuItem addHW;
	@FXML
	private MenuItem addExam;
	
	private MenuItem addQuiz2;
	private MenuItem addHW2;
	private MenuItem addExam2;
	
	private StringProperty quiz;
	private StringProperty homework;
	private StringProperty exam;
	private StringProperty total;
	
	private CompositeGrade theQuizzes;
	private CompositeGrade theHomework;
	private CompositeGrade theExams;
	
	/**
	 * Zero parameter constructor
	 */
	public SchoolGradesController() {
		this.quiz = new SimpleStringProperty("");
		this.homework = new SimpleStringProperty("");
		this.exam = new SimpleStringProperty("");
		this.total = new SimpleStringProperty("");
		
		this.addQuiz2 = new MenuItem("Add Quiz");
		this.addHW2 = new MenuItem("Add Homework");
		this.addExam2 = new MenuItem("Add Exam");
		
		this.theQuizzes = new CompositeGrade(new SumOfGradesStrategy());
		this.theHomework = new CompositeGrade(new DropLowestStrategy(new AverageOfGradesStrategy()));
		this.theExams = new CompositeGrade(new AverageOfGradesStrategy());
	}
	
	/**
	 * This will initialize the worksheet
	 */
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		this.subTotal(this.initialQuizzes(), this.quiz, this.quizTotal);
		this.subTotal(this.initialHomework(), this.homework, this.hwTotal);
		this.subTotal(this.initialExams(), this.exam, this.examTotal);
		
		this.addGrades(this.theQuizzes.getGrades(), this.quizGrade);
		this.addGrades(this.theHomework.getGrades(), this.hwGrade);
		this.addGrades(this.theExams.getGrades(), this.examGrade);
		
		this.addNewGrade(this.addQuiz, this.theQuizzes, this.quizGrade, this.quizTotal, this.quiz);
		this.addPopUp(this.quizGrade, this.theQuizzes, this.addQuiz2, this.quizTotal, this.quiz);
		this.addNewGrade(this.addHW, this.theHomework, this.hwGrade, this.hwTotal, this.homework);
		this.addPopUp(this.hwGrade, this.theHomework, this.addHW2, this.hwTotal, this.homework);
		this.addNewGrade(this.addExam, this.theExams, this.examGrade, this.examTotal, this.exam);
		this.addPopUp(this.examGrade, this.theExams, this.addExam2, this.examTotal, this.exam);
		
		this.editList(this.quizGrade, this.theQuizzes);
		this.editList(this.hwGrade, this.theHomework);
		this.editList(this.examGrade, this.theExams);
		
		this.calculate();
	}
	
	/**
	 * This will return the sum of the quizzes
	 * 
	 * @return		quizzes
	 */
	public double initialQuizzes() {
		List<Grade> grades = new ArrayList<Grade>();
		
		grades.add(new SimpleGrade(0));
		grades.add(new SimpleGrade(10));
		
		this.theQuizzes.addAll(grades);
		
		return this.theQuizzes.getValue();
	}
	
	/**
	 * This will return the average of the homework with lowest grade dropped
	 * 
	 * @return		homework
	 */
	public double initialHomework() {
		List<Grade> grades = new ArrayList<Grade>();
		
		grades.add(new SimpleGrade(100));
		grades.add(new SimpleGrade(80));
		grades.add(new SimpleGrade(60));
		grades.add(new SimpleGrade(40));
		grades.add(new SimpleGrade(20));
		
		this.theHomework.addAll(grades);
		
		return this.theHomework.getValue();
	}
	
	/**
	 * This will return the average of the exams
	 * 
	 * @return		exam
	 */
	public double initialExams() {
		List<Grade> grades = new ArrayList<Grade>();
		
		grades.add(new SimpleGrade(99));
		grades.add(new SimpleGrade(67));
		grades.add(new SimpleGrade(73));
		grades.add(new SimpleGrade(88));
		
		this.theExams.addAll(grades);
		
		return this.theExams.getValue();
	}
	
	/**
	 * Private helper to initialize the worksheet
	 * 
	 * @param grades		grades	
	 * @param property		string property
	 * @param text			textfield
	 */
	private void subTotal(double grades, StringProperty property, TextField text) {
		double grade = grades;
		String theGrade = String.format("%.2f", grade);
		property.set(theGrade);
		text.textProperty().bind(property);
	}
	
	/**
	 * Helper method to populate the worksheet
	 * 
	 * @param list			the list
	 * @param listview		listview
	 */
	public void addGrades(List<Grade> list, ListView<String> listview) {
		listview.getItems().clear();
		for (Grade current : list) {
			String grade = String.format("%.2f", current.getValue());
			listview.getItems().add(grade);
		}
	}
	
	/**
	 * This will use the menu to add a new grade
	 */
	private void addNewGrade(MenuItem item, CompositeGrade grade, ListView<String> list, TextField text, StringProperty prop) {
		item.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent arg0) {
				Grade aGrade = new SimpleGrade(0);
				grade.add(aGrade);
				list.getItems().add(String.format("%.2f", aGrade.getValue()));
				prop.set(String.format("%.2f", grade.getValue()));
				text.textProperty().bind(prop);
			}
		});
	}
	
	/**
	 * This will add a grade using a popup
	 */
	private void addPopUp(ListView<String> listview, CompositeGrade grade, MenuItem item, TextField text, StringProperty prop) {
		listview.setOnMouseClicked(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				MouseButton button = event.getButton();
				
				if (button == MouseButton.SECONDARY) {
					ContextMenu context = new ContextMenu();
	                context.getItems().add(item);
	                context.show(listview, event.getScreenX(), event.getScreenY());
	                SchoolGradesController.this.addNewGrade(item, grade, listview, text, prop);
	            }
			} 
		});
	}
	
	/**
	 * This will allow the user to edit the values in the listviews
	 * 
	 * @param list		list
	 */
	private void editList(ListView<String> list, CompositeGrade grade) {
		list.setEditable(true);
		list.setCellFactory(TextFieldListCell.forListView());
		
		list.setOnEditCommit(new EventHandler<ListView.EditEvent<String>>() {
			@Override
			public void handle(ListView.EditEvent<String> string) {
				try {
					int index = string.getIndex();
					double theGrade = Double.parseDouble(string.getNewValue());
					
					SchoolGradesController.this.editCellHelper(list, grade, index, theGrade);
				} catch (NumberFormatException nfe) {
					
				} catch (IllegalArgumentException iae) {
					
				}
			}			
		});
	}
	
	/**
	 * Helper method to modify cell contents
	 * 
	 * @param list			listview
	 * @param grade			compositegrade
	 * @param index			int
	 * @param theGrade		double
	 */
	private void editCellHelper(ListView<String> list, CompositeGrade grade, int index, double theGrade) {
		for (int current = 0; current < list.getItems().size(); current++) {
			if (current == index) {
				SimpleGrade aGrade = (SimpleGrade) grade.getGrades().get(current);
				aGrade.setValue(theGrade);
			}
		}
		
		this.addGrades(grade.getGrades(), list);
		
		if (grade == this.theQuizzes) {
			this.quiz.set(String.format("%.2f", grade.getValue()));
			this.quizTotal.textProperty().bind(this.quiz);
		} else if (grade == this.theHomework) {
			this.homework.set(String.format("%.2f", grade.getValue()));
			this.hwTotal.textProperty().bind(this.homework);
		} else if (grade == this.theExams) {
			this.exam.set(String.format("%.2f", grade.getValue()));
			this.examTotal.textProperty().bind(this.exam);
		}
	}
	
	/**
	 * This will calculate the subtotal and final grade when the button is clicked
	 */
	private void calculate() {
		this.recalculate.setOnMouseClicked(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				double quiz = Double.parseDouble(SchoolGradesController.this.quiz.get());
				double hw = Double.parseDouble(SchoolGradesController.this.homework.get());
				double exam = Double.parseDouble(SchoolGradesController.this.exam.get());
				
				double grade = (.2 * quiz) + (.3 * hw) + (.5 * exam);
				
				SchoolGradesController.this.total.set(String.format("%.3f", grade));
				SchoolGradesController.this.finalGrade.textProperty().bind(SchoolGradesController.this.total);
			}
		});
	}
	
	/**
	 * This method will return the string of the quiz grade StringProperty
	 * 
	 * @return		quiz
	 */
	public String getQuiz() {
		return this.quiz.get();
	}
	
	/**
	 * This method will return the StringProperty
	 * 
	 * @return		quiz
	 */
	public StringProperty quizTotal() {
		return this.quiz;
	}
	
	/**
	 * This method will return the String of the homework StringProperty
	 * 
	 * @return		homework
	 */
	public String getHomework() {
		return this.homework.get();
	}
	
	/**
	 * This method will return the StringProperty
	 * 
	 * @return		homework
	 */
	public StringProperty homeworkTotal() {
		return this.homework;
	}
	
	/**
	 * This method will return the String of the exam grade StringProperty
	 * 
	 * @return		exam
	 */
	public String getExam() {
		return this.exam.get();
	}
	
	/**
	 * This method will return the StringProperty
	 * 
	 * @return		exam
	 */
	public StringProperty examTotal() {
		return this.exam;
	}
	
	/**
	 * This method will return the String of the final grade StringProperty
	 * 
	 * @return		total
	 */
	public String getTotal() {
		return this.total.get();
	}
	
	/**
	 * This method will return the StringProperty
	 * 
	 * @return		total
	 */
	public StringProperty finalTotal() {
		return this.total;
	}
}
