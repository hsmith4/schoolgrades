package edu.westga.cs.schoolgrades.model;

import org.junit.Before;
import org.junit.Test;

/**
 * Test class
 * 
 * @author Hayden Smith
 * 
 * @version 11/1/17
 *
 */
public class TestDropLowestStrategyConstructor {

	@Before
	public void setUp() throws Exception {
	}

	@Test(expected = IllegalArgumentException.class)
	public void shouldNotAllowNullChildStrategy() {
		new DropLowestStrategy(null);
	}

}
