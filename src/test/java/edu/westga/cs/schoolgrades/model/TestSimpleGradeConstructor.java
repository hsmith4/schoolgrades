package edu.westga.cs.schoolgrades.model;

import static org.junit.Assert.*;

import org.junit.Test;

public class TestSimpleGradeConstructor {

	private static final double DELTA = 0.001;

	@Test(expected = IllegalArgumentException.class)
	public void shouldNotAllowNegativeGrades() {
		new SimpleGrade(-1);
	}
	
	@Test
	public void shouldAllowZeroValue() {
		SimpleGrade grade = new SimpleGrade(0);
		assertEquals(0, grade.getValue(), DELTA);
	}
	
	@Test
	public void shouldAllowPositiveValue() {
		SimpleGrade grade = new SimpleGrade(50.0);
		assertEquals(50.0, grade.getValue(), DELTA);
	}

}
