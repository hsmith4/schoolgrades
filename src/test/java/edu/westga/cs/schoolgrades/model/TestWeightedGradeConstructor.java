package edu.westga.cs.schoolgrades.model;

import org.junit.Test;

public class TestWeightedGradeConstructor {

	@Test(expected = IllegalArgumentException.class)
	public void shouldNotAllowNullGrade() {
		new WeightedGrade(null, 10.0);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void shouldNotAllowNegativeWeight() {
		new WeightedGrade(new SimpleGrade(50), -10.0);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void shouldNotAllowWeightGreaterThanOne() {
		new WeightedGrade(new SimpleGrade(50), 10.0);
	}
}
