package edu.westga.cs.schoolgrades.model;

import static org.junit.Assert.*;

import org.junit.Test;

public class TestCompositeGradeConstructor {

	
	
	@Test(expected = IllegalArgumentException.class)
	public void shouldNotAllowNullStrategy() {
		new CompositeGrade(null);
	}
	
	@Test
	public void shouldHaveNoGradesWhenCreated() {
		CompositeGrade grade = new CompositeGrade(new SumOfGradesStrategy());
		assertTrue(grade.getGrades().isEmpty());
	}

}
